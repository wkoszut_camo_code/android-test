package com.camocode.exam;

import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by wkoszut on 03/04/14.
 */
public class CoreJava {

    @Test
    public void sortObjects() {
        final String[] strings = {"z", "x", "y", "abc", "zzz", "zazzy"};
        final String[] expected = {"abc", "x", "y", "z", "zazzy", "zzz"};

        //please sort strings array in the simple possible way

        assertArrayEquals(expected, strings);
    }

    @Test
    public void arrayCopy() {
        int[] integers = {0, 1, 2, 3, 4};

        //expand existing array by 1

        assertEquals(6, integers.length);

        //set last element of array to 5

        assertEquals(5, integers[5]);
    }

    // Write an algorithm that prints all numbers between 1 and n,
    // replacing multiples of 3 with the String Bla, multiples of 5 with Blu, and multiples of 7 with BlaBlu
    @Test
    public void blaBlu() {
        String[] expctedResult = {"1", "2", "Bla", "4", "Blu", "Bla", "BlaBlu", "8", "Bla", "Blu"};

        int n = 10;
        final List<String> toReturn = new ArrayList<>(n);

        assertArrayEquals(expctedResult, toReturn.toArray());

    }


    @Test
    public void positiveList() {
        final List<Integer> numbers = Arrays.asList(4, 7, 2, -2, 8, -5, -7);
        final List<Integer> expected = Arrays.asList(4, 7, 2, 2, 8, 5, 7);
        final List<Integer> actual = null;
//        actual = updateList(
//        Implement this part of code using IntegerOperation interface to transform numbers lists to absolute numbers lists
//        );
        assertEquals(expected, actual);
    }


    public interface IntegerOperation {
        Integer performOperation(Integer value);
    }


    public static List<Integer> updateList(final List<Integer> numbers, final IntegerOperation op) {
        final ArrayList<Integer> toReturn = new ArrayList<>(numbers.size());
        for (final Integer number : numbers) {
            toReturn.add(op.performOperation(number));
        }
        return toReturn;
    }

    public static class ThreadPrinter implements Runnable {
        int num = 0;

        public ThreadPrinter(int num) {
            this.num = num;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                System.out.printf("Thread %n loop %n", num, i);
                try {
                    Thread.sleep(100l);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.printf("Thread %n ended.", num);
        }
    }

    @Test
    public void threadTester() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

//        Using Executor run 4 ThreadPrinter in parallel
        latch.await(5, TimeUnit.SECONDS);
    }

    @Test
    public void threadTesterSingleton() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

//        Using Executor run 4 ThreadPrinter in one by one
        latch.await(5, TimeUnit.SECONDS);
    }


    //    Implement HTTP requests in Java and print received content
    @Test
    public void makeBareHttpRequest() throws IOException {
        final URL url = new URL("http", "en.wikipedia.org", "/");
        int responseCode = 0;
        String responseBody = null;

        assertEquals(200, responseCode);
        assertNotNull(responseBody);
    }

}
